import { IProviderAsync } from "./ports/ProviderAsync";
import { AvaiableProviders, mapToProviders } from "./providers";
import { PayloadMessage } from "./ports/PayloadMessage"

export { AvaiableProviders, PayloadMessage, IProviderAsync }
export class Messager {
    getAsyncMessager(provider: AvaiableProviders): IProviderAsync {
        const providerFound = mapToProviders[provider];
        if (!providerFound) throw new Error(`Message provider ${provider} not avaiable`)
        return providerFound;
    }
}