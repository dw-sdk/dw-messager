import { RabbitMQProvider } from "./RabbitMQ/RabbitMQ";

export enum AvaiableProviders {
    RABBITMQ = 'RABBITMQ'
}
export const mapToProviders = {
    [AvaiableProviders.RABBITMQ]: new RabbitMQProvider()
}