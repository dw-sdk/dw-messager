import { Envs } from "../../helpers/envs";

interface RabbitMQConfigDto {
    targets: Array<{
        targetName: string;
        routingKey?: string;
        exchange: string;
    }>,
    origins: Array<{
        originName: string;
        queue: string;
        requeue?: boolean;
    }>
}

export class RabbitMQConfig {
    public configs: RabbitMQConfigDto;

    constructor() {
        this.configs = Envs.configProvider('RABBITMQ');
    }
}