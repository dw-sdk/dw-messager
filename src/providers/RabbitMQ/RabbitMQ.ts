import amqp from 'amqplib';
import { IProviderAsync } from '../../ports/ProviderAsync';
import { PayloadMessage } from '../../ports/PayloadMessage';
import { RabbitMQConfig } from './RabbitMQConfig';

export class RabbitMQProvider implements IProviderAsync {
    private connection: amqp.Connection | undefined;
    private pedingConnection: Promise<amqp.Connection> | undefined;
 
    private async getConnection() {
        if (this.connection) return this.connection;
        if (this.pedingConnection) return this.pedingConnection;

        this.pedingConnection = amqp.connect(process.env.RABBIT_URL as string)
        const conn = await this.pedingConnection;

        this.connection = conn;
        this.pedingConnection = undefined;
        return conn;
    }

    private async createChannel() {
        return (await this.getConnection()).createChannel()
    }

    async sendMessage(target: string, payload: PayloadMessage): Promise<void> {
        const channel = await this.createChannel();

        const config = new RabbitMQConfig();

        const sendTarget = config.configs.targets.find(targetObj => targetObj.targetName === target )
        if (!sendTarget) throw new Error(`Target ${target} not found`)

        try {
            channel.publish(sendTarget.exchange, sendTarget.routingKey ?? '', Buffer.from(JSON.stringify(payload)));
        } catch (error) {
            await channel.close();
            throw error;
        } finally {
            await channel.close();
        }
    }

    async receiveMessage(origin: string, func: (payload: PayloadMessage) => Promise<void>): Promise<void> {
        const config = new RabbitMQConfig();
        const numberOfChannels = process.env.RABBIT_NUMBER_CHANNELS || '1'
        
        const consumeOrigin = config.configs.origins.find(originObj => originObj.originName === origin)
        if (!consumeOrigin) throw new Error(`Origin ${origin} not found`)
        
        for (let i = 0;i < parseInt(numberOfChannels);i++) {
            const channel = await this.createChannel();
            try {
                const handleMessage = async (msg: amqp.ConsumeMessage | null) => {
                    if (msg) {
                        try {
                            const messageContent = JSON.parse(msg.content.toString());
                            await func(messageContent);
                            channel.ack(msg)
                        } catch (error: any) {
                            let isRequeue = true;
                            if (consumeOrigin.requeue !== undefined) {
                                isRequeue = consumeOrigin.requeue
                            }
                            channel.nack(msg, false, isRequeue)
                            console.log('Error process message', error.message)
                        }
                    }
                };
        
                await channel.consume(consumeOrigin.queue, handleMessage);
            } catch (error) {
                await channel.close()
                throw error;
            }
        }
    }
}