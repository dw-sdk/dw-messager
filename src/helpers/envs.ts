export class Envs {
    static configProvider(provider: string) {
        const providers = process.env.PROVIDER_MESSAGES_CONFIG;
        if (!providers) throw new Error('PROVIDER_MESSAGES_CONFIG not found')

        const config = JSON.parse(providers);
        if (!config[provider]) throw new Error(`Config not found to ${provider}`)

        return config[provider];
    }
}