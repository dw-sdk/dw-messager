export interface PayloadMessage {
    traceId: string;
    msOrigin: string;
    content: any;
}