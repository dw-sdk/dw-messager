import { PayloadMessage } from "./PayloadMessage";

export interface IProviderAsync {
    sendMessage(target: string, payload: PayloadMessage): Promise<void>;
    receiveMessage(origin: string, func: (payload: PayloadMessage) => Promise<void>): Promise<void>;
}