# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.2](https://gitlab.com/dw-sdk/dw-messager/compare/v1.3.1...v1.3.2) (2023-10-27)

### [1.3.1](https://gitlab.com/dw-sdk/dw-messager/compare/v1.3.0...v1.3.1) (2023-10-27)


### Bug Fixes

* resolve build ([f24754e](https://gitlab.com/dw-sdk/dw-messager/commit/f24754e9ad31ee01cd4763e7fe5c597f71585b75))

## [1.3.0](https://gitlab.com/dw-sdk/dw-messager/compare/v1.2.4...v1.3.0) (2023-10-27)


### Features

* add dequeue ([4d86363](https://gitlab.com/dw-sdk/dw-messager/commit/4d86363e1a8249dc695f87ed8da80e9d6a3ce225))


### Bug Fixes

* fix logic ([05aa215](https://gitlab.com/dw-sdk/dw-messager/commit/05aa2157c92e26b19110b1d7083cf52cccd87848))
* resolve logic ([635efa4](https://gitlab.com/dw-sdk/dw-messager/commit/635efa4bd56982b6a15c2c3bae6e6d8fa2d9d356))

### [1.2.4](https://gitlab.com/dw-sdk/dw-messager/compare/v1.2.3...v1.2.4) (2023-10-24)

### [1.2.3](https://gitlab.com/dw-sdk/dw-messager/compare/v1.2.1...v1.2.3) (2023-10-24)


### Bug Fixes

* resolve conflicts ([9404221](https://gitlab.com/dw-sdk/dw-messager/commit/9404221421a5e0775ff74c6058c33cf488c5eb85))

### [1.2.2](https://gitlab.com/dw-sdk/dw-messager/compare/v1.2.1...v1.2.2) (2023-10-24)


### Bug Fixes

* add types ([e9d8212](https://gitlab.com/dw-sdk/dw-messager/commit/e9d821228e9911cf177c6aac33ea92e63c78451b))

### [1.2.1](https://gitlab.com/dw-sdk/dw-messager/compare/v1.1.0...v1.2.1) (2023-10-23)

## [1.2.0](https://gitlab.com/dw-sdk/dw-messager/compare/v1.1.0...v1.2.0) (2023-10-22)


### Features

* receive message async ([28fdf8d](https://gitlab.com/dw-sdk/dw-messager/commit/28fdf8d4ae709fcba1fe552f0ca33e012be0959b))
* rename var ([068db98](https://gitlab.com/dw-sdk/dw-messager/commit/068db985b5b09241a865d902ec2bc2ea8f796294))


### Bug Fixes

* multiple connections ([c03a7a8](https://gitlab.com/dw-sdk/dw-messager/commit/c03a7a85f2ae231c3ed5aeb64fcd67095da7919a))

## 1.1.0 (2023-10-22)


### Features

* messager class created ([d5b4f2c](https://gitlab.com/dw-sdk/dw-messager/commit/d5b4f2c2a877f22a6cbeab36d2e8becbadc08983))
* RabbitProvider ([aa7fe54](https://gitlab.com/dw-sdk/dw-messager/commit/aa7fe540c82c6ffb2ed3e1e667092af7b15b2715))


### Bug Fixes

* resolve bugs ([6a05b8a](https://gitlab.com/dw-sdk/dw-messager/commit/6a05b8af877bb4ddf2743bf1f4b28a05a2e7a0e9))
